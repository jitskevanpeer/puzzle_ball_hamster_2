using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

public class Movement : MonoBehaviour
{

    public float xForce = 10.0f;
    public float zForce = 10.0f;
    public float yForce = 500.0f;
    private bool isGrounded = false;

    //use this for initialization  

    void Start()
    {

    }
    //Update is called once per frame  
    void Update()
    {
        //this is for x axis' movement  

        float x = 0.0f;
        if (Input.GetKey("left"))
        {
            x = x - xForce;
        }

        if (Input.GetKey("right"))
        {
            x = x + xForce;
        }

        //this is for z axis' movement  

        float z = 0.0f;
        if (Input.GetKey("down"))
        {
            z = z - zForce;
        }

        if (Input.GetKey("up"))
        {
            z = z + zForce;
        }

        //this is for z axis' movement  
        //check if the ball is grounded
        float y = 0.0f;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (isGrounded)
            {
                //this is for z axis' movement 
                
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    y = yForce;
                    isGrounded = false;
                }
            }
        }

        GetComponent<Rigidbody>().AddForce(x, y, z);
    }

    void OnCollisionEnter(Collision collision)
    {
        
        if (collision.gameObject.tag == "ground")
        {
            isGrounded = true;
            PlayerPrefs.SetInt("heeft geteleporteerd", 0);
        }
        if (collision.gameObject.tag == "jumppad")
        {
            GetComponent<Rigidbody>().AddForce(0,1000000*5,0);
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "ground")
        {
            isGrounded = false;
        }
    }

}
