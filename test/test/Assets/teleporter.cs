using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class teleporter : MonoBehaviour
{
    public Transform teleporterobject;
    public Transform player;
    private Vector3 initalOffset;
    private Vector3 newPlayerposition;
    // Start is called before the first frame update
    void Start()
    {
        initalOffset = transform.position - player.position;
        PlayerPrefs.SetInt("heeft geteleporteerd",0);
    }

    // Update is called once per frame
    void OnCollisionEnter(Collision collision)
    {
        initalOffset = transform.position - player.position;

        if (collision.gameObject.tag == "player" & PlayerPrefs.GetInt("heeft geteleporteerd")==0)
        {
            Debug.Log("teleporter geraakt");
            newPlayerposition = teleporterobject.position - initalOffset;
            player.position = newPlayerposition;
            PlayerPrefs.SetInt("heeft geteleporteerd", 1);
        }
    }
}
