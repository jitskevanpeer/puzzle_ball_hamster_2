MAKE A FOLDER THAT WILL CONTAIN THE REPOSITORY, THIS WILL BE LINKED WITH EVERYONE BY GIT. YOU CAN DRAG OR STORE THE FILES YOU WANT TO SHARE IN THIS FOLDER, NAME IT SOMETHING LIKE: R&D_PUZZLE_BALL_HAMSTER FOR EXAMPLE
THEN RIGHT CLICK THE FOLDER AND CHOOSE 'GIT BASH HERE'

git config --global user.name "lars.vandijk"
git config --global user.email "lars.vandijk@student.kuleuven.be"
git clone https://LarsDijk123@bitbucket.org/LarsDijk123/puzzle_ball_hamster_2.git
cd puzzle_ball_hamster_2
git status

TO PUSH SOMETHING:
git add file.ending (or 'git add .' to add everything)
git commit
git push

TO PULL SOMETHING (ALWAYS DO THIS BEFORE PUSHING):
git pull